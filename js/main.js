window.onload = () => {
    const MYSQUARE = document.querySelector('.mysquare');
    const OBJECTIVE = document.querySelector('.objectivesquare');
    const COUNTER = document.querySelector('#counter');

    let posObjective = OBJECTIVE.getBoundingClientRect();

    let moveDistance = 10;
    let movedDistanceY = 0;
    let movedDistanceX = 0;

    let numCounter = 0;

    window.addEventListener("keydown", (e) => {
        if (e.defaultPrevented) {
          return;
        }

        let differenceY = posObjective.top - movedDistanceY;
        let differenceX = posObjective.left - movedDistanceX;

        if((differenceY < 75 && differenceY > -75 ) && (differenceX < 75 && differenceX > -75 )) {
            MYSQUARE.style.backgroundColor = "white";
            OBJECTIVE.style.backgroundColor = "#0080FF";
            numCounter += 1;
        } else {
            MYSQUARE.style.backgroundColor = "#0080ff";
            OBJECTIVE.style.backgroundColor = "#6d6d6d";
        }
        
        COUNTER.innerHTML = numCounter;

        switch (e.key) {
          case "ArrowDown":
            movedDistanceY += moveDistance;
            MYSQUARE.style.top = movedDistanceY + "px";
            break;
          case "ArrowUp":
            movedDistanceY -= moveDistance;
            MYSQUARE.style.top = movedDistanceY + "px";
            break;
          case "ArrowLeft":
            movedDistanceX -= moveDistance;
            MYSQUARE.style.left = movedDistanceX + "px";
            break;
          case "ArrowRight":
            movedDistanceX += moveDistance;
            MYSQUARE.style.left = movedDistanceX + "px";
            break;
          default:
            return; 
        }
      
        e.preventDefault();
    }, true);

}